package com.larockola.miproyecticor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiProyecticoRApplication {

    public static void main(String[] args) {
        SpringApplication.run(MiProyecticoRApplication.class, args);
    }

}
